type Options = {
  /**
   * Invoked when user mouses over an element. Optional, defaults to no-op.
   *
   * This is where the overlay can be styled. Probably don't change anything
   * related to its display, position, width, or height.
   *
   * @param target Element that was moused over
   * @param overlay Overlay element that is visible and positioned over target.
   */
  mouseOver?: (target: Element, overlay: HTMLElement) => void;
  /**
   * Invoked when user mouses out from an element. Optional, defaults to no-op.
   *
   * @param target Element that was moused-out from
   */
  mouseOut?: (target: Element) => void;
};

type Picker = {
  /**
   * Cancel the pick operation.
   */
  cancel: () => void;
  /**
   * Resolved with the picked element or null if the pick was canceled.
   */
  finish: Promise<Element | null>;
};

export function pickElement(options: Options = {}): Picker {
  const mouseOverOption = options.mouseOver;
  const mouseOutOption = options.mouseOut;

  let onFinish: (element: Element | null) => void;
  const pickPromise = new Promise<Element | null>((resolve) => {
    onFinish = resolve;
  });

  const overlay = document.createElement('div');
  overlay.id = '__select-element-overlay__';
  overlay.style.zIndex = String(1000);
  overlay.style.position = 'absolute';
  overlay.style.boxSizing = 'border-box';
  overlay.style.pointerEvents = 'none';

  if (process.env.NODE_ENV === 'test') {
    overlay.dataset.testid = 'overlay';
  }
  hideOverlay(overlay);

  document.body.appendChild(overlay);

  document.addEventListener('mouseover', onMouseOver);
  document.addEventListener('mouseout', onMouseOut);
  document.addEventListener('click', onClick);

  return {
    cancel: () => endgame(null),
    finish: pickPromise,
  };

  function onMouseOver(event: MouseEvent) {
    if (event.target instanceof Element) {
      placeOverlay(overlay, event.target);
      showOverlay(overlay);
      mouseOverOption && mouseOverOption(event.target, overlay);
    }
  }

  function placeOverlay(overlay: HTMLElement, target: Element) {
    const { top, left, width, height } = target.getBoundingClientRect();
    const px = (value: number) => `${value}px`;

    overlay.style.top = px(window.scrollY + top);
    overlay.style.left = px(window.scrollX + left);
    overlay.style.width = px(width);
    overlay.style.height = px(height);
  }

  function onMouseOut(event: MouseEvent) {
    if (event.target instanceof Element) {
      mouseOutOption && mouseOutOption(event.target);
    }

    hideOverlay(overlay);
  }

  function cleanUp() {
    document.removeEventListener('mouseover', onMouseOver);
    document.removeEventListener('mouseout', onMouseOut);
    document.removeEventListener('click', onClick);

    overlay.parentElement?.removeChild(overlay);
  }

  function onClick(event: MouseEvent) {
    if (event.target instanceof Element) {
      endgame(event.target);
    }
  }

  function endgame(element: Element | null) {
    hideOverlay(overlay);
    cleanUp();
    onFinish(element);
  }

  function hideOverlay(element: HTMLElement) {
    element.style.display = 'none';
  }

  function showOverlay(element: HTMLElement) {
    element.style.display = 'block';
  }
}
