import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import { pickElement } from './index';

function setUp(innerHtml: string) {
  const container = document.createElement('div');
  container.id = 'container';
  container.innerHTML = innerHtml;
  document.body.appendChild(container);
}

describe('pickElement', () => {
  test('puts overlay element in the document', () => {
    pickElement();

    expect(screen.queryByTestId('overlay')).toBeInTheDocument();
  });

  describe('when mouse moves over elements', () => {
    test('mouse callbacks are invoked', async () => {
      setUp(`
        <div id="parent">
          <div id="child"></div>
        </div>
      `);

      const mouseOver = jest.fn();
      const mouseOut = jest.fn();

      pickElement({
        mouseOver,
        mouseOut,
      });

      const parent = document.getElementById('parent')!;
      userEvent.hover(parent);
      userEvent.unhover(parent);

      const child = document.getElementById('child')!;
      userEvent.hover(child);
      userEvent.unhover(child);

      const overlay = screen.getByTestId('overlay');

      expect(mouseOver).toHaveBeenNthCalledWith(1, parent, overlay);
      expect(mouseOut).toHaveBeenNthCalledWith(1, parent);

      expect(mouseOver).toHaveBeenNthCalledWith(2, child, overlay);
      expect(mouseOut).toHaveBeenNthCalledWith(2, child);

      expect(mouseOver).toHaveBeenCalledTimes(2);
      expect(mouseOut).toHaveBeenCalledTimes(2);
    });
  });

  describe('when element is selected', () => {
    test('picker.finish resolves with selected element', async () => {
      setUp(`
        <div id="parent">
          <div id="child"></div>
        </div>
      `);

      const picker = pickElement();

      userEvent.click(document.getElementById('child')!);

      const element = await picker.finish;
      expect(element).toBe(document.getElementById('child'));
    });

    test('overlay is no longer in document', async () => {
      setUp(`
        <div id="parent">
          <div id="child"></div>
        </div>
      `);

      const picker = pickElement();

      userEvent.click(document.getElementById('child')!);

      await picker.finish;
      expect(screen.queryByTestId('overlay')).not.toBeInTheDocument();
    });

    test('mouse callbacks are no longer invoked', async () => {
      setUp(`
        <div id="parent">
          <div id="child"></div>
        </div>
      `);

      const mouseOver = jest.fn();
      const mouseOut = jest.fn();

      const picker = pickElement({
        mouseOver,
        mouseOut,
      });

      userEvent.click(document.getElementById('child')!);

      await picker.finish;

      const parent = document.getElementById('parent')!;
      userEvent.hover(parent);
      userEvent.unhover(parent);

      const child = document.getElementById('child')!;
      userEvent.hover(child);
      userEvent.unhover(child);

      // called once because the click event causes it
      expect(mouseOver).toBeCalledTimes(1);
      expect(mouseOut).toBeCalledTimes(0);
    });
  });

  describe('when canceled via picker.cancel()', () => {
    test('picker.finish resolves with null', async () => {
      setUp(`
        <div id="parent">
          <div id="child"></div>
        </div>
      `);

      const picker = pickElement();

      picker.cancel();

      const element = await picker.finish;
      expect(element).toBeNull();
    });

    test('overlay is no longer in document', async () => {
      setUp(`
        <div id="parent">
          <div id="child"></div>
        </div>
      `);

      const picker = pickElement();

      picker.cancel();

      await picker.finish;
      expect(screen.queryByTestId('overlay')).not.toBeInTheDocument();
    });

    test('mouse callbacks are no longer invoked', async () => {
      setUp(`
        <div id="parent">
          <div id="child"></div>
        </div>
      `);

      const mouseOver = jest.fn();
      const mouseOut = jest.fn();

      const picker = pickElement({
        mouseOver,
        mouseOut,
      });

      picker.cancel();

      await picker.finish;

      const parent = document.getElementById('parent')!;
      userEvent.hover(parent);
      userEvent.unhover(parent);

      const child = document.getElementById('child')!;
      userEvent.hover(child);
      userEvent.unhover(child);

      expect(mouseOver).toBeCalledTimes(0);
      expect(mouseOut).toBeCalledTimes(0);
    });
  });

  describe('svg', () => {
    test('also works', async () => {
      setUp(`
        <svg>
          <g id="parent">
            <rect id="child" width="100" height="100" />
          </g>
        </svg>
      `);

      const picker = pickElement();

      userEvent.click(document.getElementById('child')!);

      const element = await picker.finish;
      expect(element).toBe(document.getElementById('child'));
    });
  });
});
