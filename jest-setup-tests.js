// Reset event listeners and <body> after each test.

const documentListeners = [];

beforeAll(async () => {
  const original = document.addEventListener;
  document.addEventListener = function replacement(type, listener, options) {
    // Remember parameters for clean up after the test
    documentListeners.push([type, listener, options]);

    // Actually add the listener
    original.apply(document, [type, listener, options]);
  };
});

afterEach(async () => {
  while (documentListeners.length) {
    const [type, listener, options] = documentListeners.pop();
    document.removeEventListener(type, listener, options);
  }

  document.body.innerHTML = '';
});
