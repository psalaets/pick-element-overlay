# Change Log

## [2.0.1] - 2021-11-20

### Fixed

- box-sizing issue with overlay element.

## [2.0.0] - 2021-10-26

Typing changes to work with svg.

### Breaking

- Options parameter type changed: `Options.mouseOver` is now an optional `(target: Element, overlay: HTMLElement) => void`, was optional `(target: Element, overlay: Element) => void`.
- Picker return type changed: `Picker.finish` is now `Promise<Element | null>`, was `Promise<HTMLElement | null`.

## [1.2.0] - 2021-10-25

### Changed

- Docs update

## [1.1.0] - 2021-10-25

Initial version
