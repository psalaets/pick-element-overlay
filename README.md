# pick-element-overlay

Move an overlay around as user picks an element with a mouse.

## Install

```
$ yarn add pick-element-overlay
```

or

```
$ npm install pick-element-overlay
```

## Usage

```ts
import { pickElement } from 'pick-element-overlay';

const picker = pickElement({
  mouseOver(target, overlay) {
    // Add styles and/or children to overlay here
  },
});

picker.finish.then((element) => {
  if (element) {
    // user picked element
  } else {
    // pick operation was canceled
  }
});

// cancels pick operation
picker.cancel();
```

## API

### pickElement(options = {})

#### options

```ts
type Options = {
  /**
   * Invoked when user mouses over an element. Optional, defaults to no-op.
   *
   * This is where the overlay can be styled. Probably don't change anything
   * related to its display, position, width, or height.
   *
   * @param target Element that was moused over
   * @param overlay Overlay element that is visible and positioned over target.
   */
  mouseOver?: (target: Element, overlay: HTMLElement) => void;
  /**
   * Invoked when user mouses out from an element. Optional, defaults to no-op.
   *
   * @param target Element that was moused-out from
   */
  mouseOut?: (target: Element) => void;
};
```

#### Returns

A picker object.

```ts
type Picker = {
  /**
   * Cancel the pick operation.
   */
  cancel: () => void;
  /**
   * Resolved with the picked element or null if the pick was canceled.
   */
  finish: Promise<Element | null>;
};
```

## License

MIT
